package Spider

import (
	"encoding/json"
	"fmt"
	"github.com/neighborhood999/fiveN1-rent-scraper"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
)

type FiveN1 struct {
	records  int
	pages    int
	queryURL string
	RentList rent.HouseInfoCollection
	wg       sync.WaitGroup
	rw       sync.RWMutex
	client   *http.Client
	cookie   *http.Cookie
}
type Rentdata struct {
	Preview    string `json:"preview"`
	Title      string `json:"title"`
	URL        string `json:"url"`
	Address    string `json:"address"`
	RentType   string `json:"rentType"`
	OptionType string `json:"optionType"`
	Ping       string `json:"ping"` // a.k.a 坪數
	Floor      string `json:"floor"`
	Price      string `json:"price"`
	IsNew      bool   `json:"isNew"`
}
type Total struct {
	page []*Page
}
type Page struct {
	rentdata map[string][]*Rentdata
}

func createurl() *rent.Options {
	return &rent.Options{Region: 1, FirstRow: 0, Kind: 0, HasImg: "1"}
}
func Option(page int) {
	options := createurl()

	url, err := rent.GenerateURL(options)
	if err != nil {
		log.Fatalf("\x1b[91;1m%s\x1b[0m", err)
	}

	f := rent.NewFiveN1(url)

	if err := f.Scrape(page); err != nil {
		log.Fatal(err)
	}

	jsonData := rent.ConvertToJSON(f.RentList)
	var total Total
	err = json.Unmarshal(jsonData, &total)
}

//下面開始我自己寫的
func Client(url string) *http.Response {
	client := &http.Client{}
	w, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("Client failed%s\n", err)
	}

	w.Header.Add("Content-Type", "application/json; charset=utf-8")
	w.Header.Add("Access-Control-Allow-Origin", "*")
	w.Header.Add("Access-Control-Allow-Credentials", "true")
	w.Header.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header.Add("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token")
	w.Header.Add("User-Agent", "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Chrome/85.0.4183.83‡ Safari/537.36")

	Res, err := client.Do(w)
	if Res.StatusCode != 200 {
		log.Printf("Status Code err%d\n%s", Res.StatusCode, Res.Status)
		os.Exit(1)
	}
	if err != nil {
		log.Printf("Get url header filed%s\n", err)
		os.Exit(1)
	}
	return Res
}
func Renthtml(url string) {
	renthtml := Client(url)

	response, err := ioutil.ReadAll(renthtml.Body)
	defer renthtml.Body.Close()

	if err != nil {
		log.Printf("ioutil ReadAll%s\n", err)
		os.Exit(1)
	}

	fmt.Println(string(response))

}
