package BLC

import (
	"encoding/hex"
	"fmt"
	"math/big"
	"time"
)

func (database *Database) Withfirstblock(data string) {
	if database.ROW.Next() == false {
		fmt.Println("生成創世區塊練")
		cbtx := NewCoinbaseTX(data, "This Time Chancellor on bank of second bailout for banks")
		wordblock := Firstwordblock(cbtx)
		database.AddGenesisBlockToDB(wordblock.Serialize(), fmt.Sprintf("%x", wordblock.Hash))
		database.Tip = wordblock.Hash
		fmt.Println("創世區塊練生成完畢")
	} else {
		fmt.Println("創世區塊練已經存在")
	}
}
func (database *Database) FindUnspentTransaction(address string, txs []*Transaction) []Transaction {
	var (
		hashInt    big.Int
		unspentTXs []Transaction
	)
	spentTXOs := make(map[string][]int)

	for _, tx := range txs {

		txID := hex.EncodeToString(tx.Hash) //byte to array
	Output: //指標
		for outIdx, out := range tx.Vout {
			//交易是否有被使用?
			if spentTXOs[txID] != nil {
				for _, spentOut := range spentTXOs[txID] {
					if spentOut == outIdx {
						continue Output
					}
				}
			}
			if out.CanBeUnlcok(address) {
				unspentTXs = append(unspentTXs, *tx)
			}
		}
		//
		if tx.Isconinbase() == false {
			for _, in := range tx.Vin {
				if in.CanUnlcokOutput(address) {
					inTXid := hex.EncodeToString(in.Hash)
					spentTXOs[inTXid] = append(spentTXOs[inTXid], in.Vout)
				}
			}
		}
	}

	bci := database.Interator()
	for {
		block := bci.Next()
		for _, tx := range block.Transactions {
			//	fmt.Printf("Transactions:%x\n", tx.ID)

			txID := hex.EncodeToString(tx.Hash) //byte to array
		Outputs: //指標
			for outIdx, out := range tx.Vout {
				//交易是否有被使用?
				if spentTXOs[txID] != nil {
					for _, spentOut := range spentTXOs[txID] {
						if spentOut == outIdx {
							continue Outputs
						}
					}
				}
				if out.CanBeUnlcok(address) {
					unspentTXs = append(unspentTXs, *tx)
				}
			}
			//
			if tx.Isconinbase() == false {
				for _, in := range tx.Vin {
					if in.CanUnlcokOutput(address) {
						inTXid := hex.EncodeToString(in.Hash)
						spentTXOs[inTXid] = append(spentTXOs[inTXid], in.Vout)
					}
				}
			}
		}
		hashInt.SetBytes(block.LastBlockhash)

		if big.NewInt(0).Cmp(&hashInt) == 0 {
			break
		}
	}
	return unspentTXs
}
func (data *Database) FindSpendableOutputs(adress string, amount int, txs []*Transaction) (int, map[string][]int) {
	unspentOutputs := make(map[string][]int)
	//查看為花費
	unspentTXS := data.FindUnspentTransaction(adress, txs)
	//統計 unspentOutputs裡面的TXoutput所對應的總量
	accumulated := 0

Work:
	//遍歷交易數組
	for _, tx := range unspentTXS {
		txID := hex.EncodeToString(tx.Hash)
		//遍歷交易裡面的vout
		for outIdx, out := range tx.Vout {
			if out.CanBeUnlcok(adress) && accumulated < amount {
				accumulated += out.Value
				unspentOutputs[txID] = append(unspentOutputs[txID], outIdx)
				if accumulated >= amount {
					break Work
				}
			}
		}
	}
	return accumulated, unspentOutputs
}

//根據交易的數組，建立新的區塊
func (database *Database) MineBlock(tx []*Transaction) {
	if database.ROW.Next() == true {
		t1 := time.Now()
		fmt.Println("正在生成新的區塊")
		block := DeserializeBlock(database.Getlastblock())
		newblock := NewBlock(tx, block.BlockHeight+1, block.Hash)
		database.AddnewBlockToDB(newblock.Serialize(), fmt.Sprintf("%x", newblock.Hash))
		fmt.Println("區塊生成完畢  花費時間:", time.Since(t1))
	} else {
		fmt.Println("未有任何區塊，請先創造創世區塊")
	}
}
