package BLC

import (
	"C"
	"bytes"
	"crypto/sha256"
	"fmt"
	"math/big"
)

const targetbit = 15 //256位Hash里面前面至少要有多少個零 數字越大挖礦越難

type ProofWork struct {
	Block  *Block   //當前要驗證的區塊
	target *big.Int //大數據存儲以防溢位
}

//是否有效||重複驗證
func (proofWork *ProofWork) IsValid() bool {
	var hashInt big.Int
	hashInt.SetBytes(proofWork.Block.Hash)
	if proofWork.target.Cmp(&hashInt) == 1 {
		return true
	}
	return false
}
func (proofWork *ProofWork) Run() ([]byte, int64) {
	//將block的屬性拼接成字節
	//生成hash
	//以無線迴圈判斷hash有效值,如滿足則跳出
	nonce := 0
	var hashInt big.Int
	var hash [32]byte //hash 定義拉出迴圈
	for {
		dataBytes := proofWork.prepareData(nonce) //準備data
		hash = sha256.Sum256(dataBytes)           //生成hash
		fmt.Printf("\r%x", hash)

		hashInt.SetBytes(hash[:]) //hash to hashInt
		if proofWork.target.Cmp(&hashInt) == 1 {
			break
		} //判斷hashInt是否小於BLOCK裡面的Target }
		nonce = nonce + 1
	}
	return hash[:], int64(nonce)
}
func (pow *ProofWork) prepareData(nonce int) []byte {
	data := bytes.Join(
		[][]byte{
			pow.Block.LastBlockhash,
			pow.Block.HashTransactions(),
			IntToHex(pow.Block.Timestmp),
			IntToHex(int64(targetbit)),
			IntToHex(int64(nonce)),
			IntToHex(int64(pow.Block.BlockHeight)),
		},
		[]byte{},
	)
	return data
}
func NewProofWork(block *Block) *ProofWork { //創建新的工作證明對象 																			//創建新的工作量證明對象
	target := big.NewInt(1)                    //產生初值為1的big.Int 值以便後續左移
	target = target.Lsh(target, 256-targetbit) //左移256 - targetbit位
	return &ProofWork{block, target}
}
