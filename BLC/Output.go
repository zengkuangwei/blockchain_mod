package BLC

import (
	"database/sql"
	"encoding/hex"
	"fmt"
	"math/big"
	"time"
)

type BlockchainIterator struct {
	CunrrentHash string
	DB           *sql.DB
}

/*
func (database *Database) GetallData() {
	var data []byte
	db = database.DB
	row, _ := db.Query("SELECT `textdata`  FROM `block` ;")
	for row.Next() { //依序輸出
		row.Scan(&data)
		block := DeserializeBlock(data)
		fmt.Printf("\nBlock:%d\n", block.BlockHeight)
		fmt.Printf("LastBlockhash:%x\n", block.LastBlockhash)
		fmt.Printf(" Data:%x\n", block.Transactions)
		fmt.Printf(" Hash:%x\n", block.Hash)
		fmt.Printf(" Timestmp:%s\n", (time.Unix(block.Timestmp, 0).Format("2006-01-02 03:04:05 PM"))) //把64bit的時間格式轉回來
		fmt.Printf("Nonce:%d\n", block.Nonce)
	}
}*/

//把所有的block的訊息印出(疊代器
func (database *Database) Printchain() {
	blockchainIterator := database.Interator()
	//fmt.Printf("%s\n",blockchainIterator.CunrrentHash)

	for {
		block := blockchainIterator.Next()
		fmt.Printf("\n\t  Bi:%d\n", block.BlockHeight)
		fmt.Printf("LastBlockhash:%x\n", block.LastBlockhash)
		fmt.Printf("Hash:%x\n", block.Hash)
		fmt.Printf("Timestmp:%s\n", (time.Unix(block.Timestmp, 0).Format("2006-01-02 15:04:05 "))) //把64bit的時間格式轉回來
		fmt.Printf("\t\tNonce:%d\n", block.Nonce)
		for _, tx := range block.Transactions {
			fmt.Println("當前交易的Hash")
			fmt.Println(hex.EncodeToString(tx.Hash))
			fmt.Println("已花费的TXOuput")
			for _, in := range tx.Vin {
				fmt.Printf("TxHash:%s\tVout:%d\tScriptSig:%s\n", hex.EncodeToString(in.Hash), in.Vout, in.ScripSig)
			}
			fmt.Println("Vout: - TXOutput")
			for _, out := range tx.Vout {
				fmt.Printf("Value: %d\tScriptPubKey:%s\n", out.Value, out.ScriptPubKey)
			}

		}
		var hashInt big.Int
		hashInt.SetBytes(block.LastBlockhash)

		if big.NewInt(0).Cmp(&hashInt) == 0 {
			break
		}
	}
}
func (database *Database) Interator() *BlockchainIterator { //疊代器
	return &BlockchainIterator{fmt.Sprintf("%x", database.Tip), database.DB}
}
func (blockchainIterator *BlockchainIterator) Next() *Block {
	var (
		currentBlockBytes []byte
		block             *Block
	)
	//fmt.Printf("%s\n", blockchainIterator.CunrrentHash)

	blockchainIterator.DB.QueryRow("SELECT `textdata` FROM `block`WHERE `hash` = ?  ;", blockchainIterator.CunrrentHash).Scan(&currentBlockBytes)
	//當前疊代器hash所對應的區塊
	block = DeserializeBlock(currentBlockBytes)
	//回傳上一個區塊的hash
	blockchainIterator.CunrrentHash = fmt.Sprintf("%x", block.LastBlockhash)

	return block
}
