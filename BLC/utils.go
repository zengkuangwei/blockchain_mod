package BLC

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

//將int64 to bytes
func IntToHex(num int64) []byte {
	buff := new(bytes.Buffer)
	binary.Write(buff, binary.BigEndian, num)
	return buff.Bytes()
}

//將string to bytes
func SToB(lastBlockhash string) []byte {
	o := 0
	hash := make([]byte, len(lastBlockhash))
	for i := 0; i < len(lastBlockhash); i++ {
		str := string(lastBlockhash[i])
		if i+1 < len(lastBlockhash) {
			i++
			str += string(lastBlockhash[i])
		}
		p, _ := strconv.ParseInt(str, 16, 32) //16進(string)to10進(int64)
		hash[o] = byte(p)
		o++
	}
	return hash[:len(lastBlockhash)/2]
}
func TrunTransactions(txs []*Transaction) (out []*Transaction) {
	for i := len(txs) - 1; i >= 0; i-- {
		fmt.Print(i)
		out = append(out, txs[i])
	}
	return out[:]
}

//string Split to  array
func JsonToArray(jsonString string) []string { //寫入方法 [zengkuangwei,zengkuangwei]
	v := []string{"[", "]", "`", "'", "{", "}"}
	for i := 0; i < len(v); i++ {
		jsonString = strings.Replace(jsonString, v[i], "", -1)
	}
	return strings.Split(jsonString, ",")
}
func Newbase65() {

}

//錯誤碼輸出並關閉
func CheckError(err error) {
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}
}

//字結數組反轉

func ReverseBytes(data []byte) {
	for i, j := 0, len(data)-1; i < j; i, j = i+1, j-1 {
		data[i], data[j] = data[j], data[i]
	}
}

//CLI的例外處理
func Exception(data string) {
	fmt.Println(data)
	ptintUsage()
	os.Exit(1)
}
