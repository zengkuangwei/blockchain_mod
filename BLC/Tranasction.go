package BLC

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"fmt"
)

const subsidy = 10

type Transaction struct {
	//交易ID
	Hash []byte
	//交易輸入
	Vin []TXInput
	//交易輸出
	Vout []TXOutput
}

func (tt *Transaction) Isconinbase() bool {

	return len(tt.Vin) == 1 && tt.Vin[0].Vout == -1 && len(tt.Vin[0].Hash) == 0
}

//建立轉帳交易
func NewUTXOTransaction(from, to string, amout int, database *Database, txs []*Transaction) *Transaction {
	//輸入
	var inputs []TXInput
	//輸出
	var outputs []TXOutput

	//找到有效可用的交易輸出數據模型
	//查詢為花費的輸出
	acc, validOutputs := database.FindSpendableOutputs(from, amout, txs)
	if acc < amout {
		fmt.Println("E R R O R:Not enough money")
	}
	//建立輸入
	for txid, outs := range validOutputs {
		txID, err := hex.DecodeString(txid)
		CheckError(err)
		for _, out := range outs {
			//創建一個輸入
			input := TXInput{txID, out, from}
			//將輸入添加到inputs裡
			inputs = append(inputs, input)
		}
	}
	//建立輸出(轉帳)
	output := TXOutput{amout, to}
	outputs = append(outputs, output)
	//建立輸出(找零)
	output = TXOutput{acc - amout, from}
	outputs = append(outputs, output)
	//創建交易

	tx := Transaction{nil, inputs, outputs}
	tx.SetID()
	return &tx
}

//交易hash
func (tt *Transaction) SetID() {
	var encoded bytes.Buffer
	var hash [32]byte

	enc := gob.NewEncoder(&encoded)
	err := enc.Encode(tt)
	CheckError(err)

	hash = sha256.Sum256(encoded.Bytes())
	tt.Hash = hash[:]
}
func NewCoinbaseTX(to, data string) *Transaction {
	if data == "" {
		data = fmt.Sprintf("Reward to %s", to)
	}
	//建立輸入
	txin := TXInput{[]byte{}, -1, data}
	//建立輸出
	txout := TXOutput{1000, to}
	//創建交易
	tt := Transaction{nil, []TXInput{txin}, []TXOutput{txout}}
	tt.SetID()
	return &tt
}

//交易輸入
type TXInput struct {
	//交易ID 上一個TXOutput的value
	Hash []byte
	//存TXOutput在Vout裡的索引
	Vout int
	//用戶名
	ScripSig string
}

//檢查帳號地址是否跟輸入的相同，解鎖
func (ttin *TXInput) CanUnlcokOutput(unlockingData string) bool {
	return ttin.ScripSig == unlockingData
}

//交易輸出
type TXOutput struct {
	Value        int    //計算單位
	ScriptPubKey string //目的地用戶名
}

//檢查是否成解鎖帳號
func (ttout *TXOutput) CanBeUnlcok(unlockingData string) bool {
	return ttout.ScriptPubKey == unlockingData
}
