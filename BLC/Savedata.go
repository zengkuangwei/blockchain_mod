package BLC

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
)

type Database struct {
	DB  *sql.DB
	ROW *sql.Rows
	Tip []byte
}

const (
	host     = "localhost"
	database = "test"
	user     = "root"
	password = "1234"
	data     = "CREATE TABLE IF NOT EXISTS `block`(`hash` CHAR(64) DEFAULT NULL,`textdata` VARBINARY(5000) DEFAULT NULL) ENGINE=InnoDB "
	prehash  = "CREATE TABLE IF NOT EXISTS `Hash`(`lasthash` CHAR (64) DEFAULT NULL,`id`INT DEFAULT NULL) ENGINE=InnoDB "
)

var connectionString = fmt.Sprintf("%s:%s@(%s:3306)/%s?parseTime=true", user, password, host, database)

func (database *Database) Getlastblock() []byte {
	var block []byte

	hash := fmt.Sprintf("%x", database.Tip)

	err := database.DB.QueryRow("SELECT `textdata` FROM `block`WHERE `hash` = ?  ;", hash).Scan(&block)

	CheckError(err)

	fmt.Printf("\nGet prehash\n")
	return block
}
func (database *Database) AddnewBlockToDB(data1 []byte, Blockhash string) {

	rss, err := database.DB.Prepare("INSERT INTO `block` (textdata,hash)value (?,?);") //儲存資料進mysql
	rs, err := rss.Exec(data1, Blockhash)                                              //儲存資料進mysql
	CheckError(err)
	_, err = database.DB.Exec("UPDATE `hash` SET `lasthash`= ? WHERE id = ?;", Blockhash, 1) //更新

	CheckError(err)

	rowCount, err := rs.RowsAffected()

	CheckError(err)

	log.Printf("inserted %d rows", rowCount)
}
func (database *Database) AddGenesisBlockToDB(data1 []byte, Blockhash string) {

	rss, err := database.DB.Prepare("INSERT INTO `block` (textdata,hash)value (?,?);") //儲存資料進mysql

	rs, err := rss.Exec(data1, Blockhash) //儲存資料進mysql

	CheckError(err)

	rowCount, _ := rs.RowsAffected()

	log.Printf("inserted %d rows", rowCount)

	rss, _ = database.DB.Prepare("INSERT INTO `hash` (id,lasthash)value (?,?);") //儲存資料進mysql
	_, err = rss.Exec(1, Blockhash)                                              //儲存資料進mysql
}
func Checkdb() {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@(%s:3306)/?parseTime=true", user, password, host)) //連線mysql測試
	CheckError(err)
	db.Ping() //真正連線mysql
	_, err = db.Exec(fmt.Sprintf("CREATE database IF NOT EXISTS `%s`", database))
	if err != nil {
		fmt.Print(err)
		os.Exit(1)
	} else {
		fmt.Printf("Database is ready")
	}
}
func Checktable() *Database {
	var hash string
	db, err := sql.Open("mysql", connectionString) //連線mysql測試

	CheckError(err)

	db.Ping() //真正連線mysql

	_, err = db.Exec(data) //建立table(在沒有block的table的情況下) 使用Exec 可以使用mysql的語法
	CheckError(err)
	_, err = db.Exec(prehash)
	CheckError(err)

	row, _ := db.Query("SELECT hash FROM block")

	db.QueryRow("SELECT `lasthash` FROM `hash` WHERE `id` limit 0,1;").Scan(&hash)
	return &Database{db, row, SToB(hash)}
}
