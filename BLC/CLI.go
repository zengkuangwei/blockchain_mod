package BLC

import (
	"../Spider"
	"flag"
	"fmt"
	"os"
	"strconv"
)

type CLI struct {
	BC *Database
}

func ptintUsage() {
	fmt.Println("Usage:")
	fmt.Println("\tGetbalance -address ADDRESS -查詢餘額")
	fmt.Println("\tCreateblockchain -address ADDRESS - 交易數據")
	fmt.Println("\tSend -from FROM -to TO -amount AMOUNT -交易由FROM給TO amout的錢")
	fmt.Println("\tPrintchain\t\t-\t\t   - 輸出區塊訊息")
	fmt.Println("\tSignup\t\t\t-\t\t   - 註冊(功能未存在)")
	fmt.Println("\tCheckdb\t-確認database")
	fmt.Println("\t591Spider -租屋網站查詢")

}
func isValidArgs() {
	if len(os.Args) < 2 {
		ptintUsage()
		os.Exit(1)
	}
}
func get591() {
	//s:=fmt.Sprintf("https://rent.591.com.tw/?kind=0&region=8&rentprice=0&pattern=0&area=0,0")
	Spider.Option(1)
}

func (cli *CLI) createGenesisBlockchain(genesis string) {
	cli.BC.Withfirstblock(genesis)
}
func (cli *CLI) sendToken(from, to, amouts []string) {
	//新建一個交易
	var inpusts []*Transaction
	var txs []*Transaction = nil
	for i := 0; i < len(to); i++ {
		amout, _ := strconv.Atoi(amouts[i]) //string to int
		tx := NewUTXOTransaction(from[i], to[i], amout, cli.BC, txs)
		inpusts = append(inpusts, tx)
		txs = TrunTransactions(inpusts) //陣列反轉
		fmt.Printf("\r成功交易了%d個交易", i+1)
	}
	if len(from) == 0 {
		os.Exit(1)
	}
	cli.BC.MineBlock(txs) //最一開始的交易要放後面 最新的要放前面
}
func (cli *CLI) getbalance(data string) {
	count, outputmap := cli.BC.FindSpendableOutputs(data, 2147483647, nil) //2147483647是int 在32位元的極限職
	fmt.Println(count)
	fmt.Println(outputmap)
}

func (cli *CLI) ptinrchain() {
	cli.BC.Printchain()
}

func (cli *CLI) Run() {
	cli.BC = Checktable()
	isValidArgs()
	createblockchainCmd := flag.NewFlagSet("Createblockchain", flag.ExitOnError)
	GetbalanceCmd := flag.NewFlagSet("Getbalance", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("Printchain", flag.ExitOnError)
	spiderCmd := flag.NewFlagSet("591Spider", flag.ExitOnError)
	SignUpCmd := flag.NewFlagSet("Signup", flag.ExitOnError)
	sendCmd := flag.NewFlagSet("Send", flag.ExitOnError)
	ckdbCmd := flag.NewFlagSet("Checkdb", flag.ExitOnError)

	sendFrom := sendCmd.String("from", "", "原地址")
	sendTo := sendCmd.String("to", "", "目的地址")
	sendAmount := sendCmd.String("amount", "", "轉帳金額")
	SignUpaccount := SignUpCmd.String("account", "", "輸入帳號")
	SignUppassword := SignUpCmd.String("password", "", "輸入密碼")
	balanceAdress := GetbalanceCmd.String("address", "", "")
	genenisAdress := createblockchainCmd.String("address", "Gensis block data....", "創世區塊交易數據")

	switch os.Args[1] {
	case "591Spider":
		CheckError(spiderCmd.Parse(os.Args[2:]))
	case "Checkdb":
		CheckError(ckdbCmd.Parse(os.Args[2:]))
	case "Send":
		CheckError(sendCmd.Parse(os.Args[2:]))
	case "Createblockchain":
		CheckError(createblockchainCmd.Parse(os.Args[2:]))
	case "Getbalance":
		CheckError(GetbalanceCmd.Parse(os.Args[2:]))
	case "Printchain":
		CheckError(printChainCmd.Parse(os.Args[2:]))
	case "Signup":
		//err := SignUpCmd.Parse(os.Args[2:])
		CheckError(SignUpCmd.Parse(os.Args[2:]))
	default:
		ptintUsage()
		os.Exit(1)
	}
	if createblockchainCmd.Parsed() { //創建區塊練
		if *genenisAdress == "" {
			Exception("交易數據未輸入...")
		}
		cli.createGenesisBlockchain(*genenisAdress)
	}
	if GetbalanceCmd.Parsed() { //查詢餘額
		if *balanceAdress == "" {
			Exception("交易數據未輸入...")
		}
		cli.getbalance(*balanceAdress)
	}
	if sendCmd.Parsed() {
		if *sendTo == "" || *sendFrom == "" {
			Exception("交易數據錯誤")
		}
		fromeAdress := JsonToArray(*sendFrom)
		toAdress := JsonToArray(*sendTo)
		sendAmounts := JsonToArray(*sendAmount)
		fmt.Println("\nfrom\n", fromeAdress)
		fmt.Println("\nto\n", toAdress)
		fmt.Println("\nsend\n", sendAmounts)
		cli.sendToken(fromeAdress, toAdress, sendAmounts)
	}
	if ckdbCmd.Parsed() {
		Checkdb()
	}
	if printChainCmd.Parsed() {
		cli.ptinrchain()
	}
	if SignUpCmd.Parsed() {
		if *SignUpaccount == " " || *SignUppassword == "" {
			Exception("帳號密碼輸入錯誤")
		}
		fmt.Printf("輸入成功您的帳號是:%s", *SignUpaccount)
		fmt.Printf("輸入成功您的密碼是:%s", *SignUppassword)
	}
	if spiderCmd.Parsed() {
		get591()
	}
}
