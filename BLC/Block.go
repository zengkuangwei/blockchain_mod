package BLC

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"log"
	"time"
)

type Block struct {
	BlockHeight   int64          //1區塊高度
	LastBlockhash []byte         //上一個區塊Hash
	Transactions  []*Transaction //交易數據
	Timestmp      int64          //創造時間搓
	Hash          []byte         //hash
	Nonce         int64          //工作量證明
}

func (block0 *Block) Serialize() []byte {

	var result bytes.Buffer

	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(block0)
	if err != nil {
		log.Panic(err)
	}

	return result.Bytes()
}

// 反序列化
func DeserializeBlock(blockBytes []byte) *Block {

	var block Block

	decoder := gob.NewDecoder(bytes.NewReader(blockBytes))
	err := decoder.Decode(&block)
	if err != nil {
		log.Panic(err)
	}

	return &block
}

//提供給挖礦時使用  將區塊裡面所有的交易的id連接,生成hash
func (b *Block) HashTransactions() []byte {
	var (
		txHashes [][]byte
		txHash   [32]byte
	)
	for _, tx := range b.Transactions {
		txHashes = append(txHashes, tx.Hash)
	}
	txHash = sha256.Sum256(bytes.Join(txHashes, []byte{}))

	return txHash[:]
}

//把第一個區塊分別出來
func Firstwordblock(coinbase *Transaction) *Block {
	//[]*Transaction{coinbase}把CONINBASE轉成陣列型態
	firstblock := NewBlock([]*Transaction{coinbase}, 1, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
	return firstblock
}
func NewBlock(tt []*Transaction, height int64, lastBlockhash []byte) *Block { //*Block  返回Block值
	//創建區塊
	block := &Block{height, lastBlockhash, tt, time.Now().Unix(), nil, 0}
	//newblock1.sethash()

	pow := NewProofWork(block)
	hash, nonce := pow.Run()
	block.Hash = hash[:]
	block.Nonce = nonce
	return block
}
