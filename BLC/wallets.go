package BLC

import (
	"fmt"
)

type Wallets struct{ Wallets map[string]*Wallet }

//創建錢包
func NewWallets() *Wallets {

	newWallets := &Wallets{}
	newWallets.Wallets = make(map[string]*Wallet)
	return newWallets
}

func (w *Wallets) CreateNewWallet() {
	wallet := NewWallet()
	fmt.Println("Address:%s\n", wallet.GetAddress())
	w.Wallets[string(wallet.GetAddress())] = wallet

}
